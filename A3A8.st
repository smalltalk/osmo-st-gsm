"
 (C) 2010 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: A3A8 [
    <category: 'OsmoGSM-Encryption'>

    COMP := nil.
    A3A8 class >> initialize [
        DLD addLibrary: 'liba3a8'
    ]

    A3A8 class >> comp [
        "Return an array with blocks to run GSM crypto"
        ^ COMP ifNil: [
            COMP := Array new: 3.
            COMP at: 1 put: [:key :rand :res | self int_COMP128_v1: key rand: rand res: res ].
            COMP at: 2 put: [:key :rand :res | self int_COMP128_v2: key rand: rand res: res ].
            COMP at: 3 put: [:key :rand :res | self int_COMP128_v3: key rand: rand res: res ].

            COMP.
        ]
    ] 

    A3A8 class >> COMP128: aVersion ki: aKI rand: aRand [
        | str |

        aKI size = 16 ifFalse: [
            ^ self error: 'KI needs to be 16 bytes'
        ].

        aRand size = 16 ifFalse: [
            ^ self error: 'RAND needs to be 16 bytes'
        ].


        str := ByteArray new: 16.
        (self comp at: aVersion)
            value: aKI
            value: aRand
            value: (CObject new storage: str).

        ^ str
    ]

    A3A8 class >> int_COMP128_v1: aKI rand: aRand res: aRes [
        <cCall: 'COMP128_1' returning: #void args: #(#string #string #cObject)>
    ]

    A3A8 class >> int_COMP128_v2: aKI rand: aRand res: aRes [
        <cCall: 'COMP128_2' returning: #void args: #(#string #string #cObject)>
    ]

    A3A8 class >> int_COMP128_v3: aKI rand: aRand res: aRes [
        <cCall: 'COMP128_3' returning: #void args: #(#string #string #cObject)>
    ]
]

Eval [
    A3A8 initialize.
]
