"
 (C) 2010-2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: BSSAPHelper [
    <category: 'OsmoGSM'>
    <comment: 'BSSAP message routines'>

    BSSAPHelper class >> msgManagemnt [ <category: 'spec'> ^ 0 ]
    BSSAPHelper class >> msgDtap      [ <category: 'spec'> ^ 1 ]

    BSSAPHelper class >> prependManagement: aMsg [
        <category: 'creation'>
        "Prepent the BSSAP Management header"
        | tmp |

        tmp := OrderedCollection new.
        tmp add: self msgManagemnt.
        tmp add: aMsg size.
        aMsg prependByteArray: tmp asByteArray.
    ]

    BSSAPHelper class >> prependDTAP: aMsg dlci: sapi [
        <category: 'creation'>
        "Prepend the DTAP header"
        | tmp |
        tmp := OrderedCollection new.
        tmp add: self msgDtap.
        tmp add: sapi.
        tmp add: aMsg size.
        aMsg prependByteArray: tmp asByteArray.
    ]
]

Object subclass: BSSAPMessage [
    <category: 'OsmoGSM'>

    BSSAPMessage class >> decode: aStream [
        | type |
        type := aStream next.

        BSSAPMessage allSubclassesDo: [:each |
            each msgType = type
                ifTrue: [
                    ^ each parseFrom: aStream.
            ]
        ].

        ^ Error signal: 'No handler for: ', type asString.
    ]
]

BSSAPMessage subclass: BSSAPManagement [
    | data |

    <category: 'OsmoGSM'>

    BSSAPManagement class >> msgType [ <category: 'factory'> ^ BSSAPHelper msgManagemnt ]
    BSSAPManagement class >> initWith: data [
        ^ (self new)
            data: data;
            yourself.
    ]

    BSSAPMessage class >> parseFrom: aStream [
        | size data |
        size := aStream next.
        data := aStream next: size.

        ^ BSSAPManagement initWith: data.
    ]

    data: aPayload [
        data := aPayload.
    ]

    data [
        ^ data
    ]

    writeOn: aMsg [
        | dat |
        aMsg putByte: BSSAPHelper msgManagemnt.

        dat := data toMessageOrByteArray.
        aMsg putByte: dat size.
        aMsg putByteArray: dat.
    ]
]

BSSAPMessage subclass: BSSAPDTAP [
    | data li |

    <category: 'OsmoGSM'>

    BSSAPDTAP class >> msgType [ <category: 'factory'> ^ BSSAPHelper msgDtap ]
    BSSAPDTAP class >> initWith: data linkIdentifier: li [
        ^ self new
            data: data;
            linkIdentifier: li;
            yourself
    ]

    BSSAPDTAP class >> parseFrom: aStream [
        | li size dat |
        li := aStream next.
        size := aStream next.
        dat := aStream next: size.

        ^ BSSAPDTAP initWith: dat linkIdentifier: li.
    ]

    writeOn: aMsg [
        | dat |

        dat := data toMessageOrByteArray.

        aMsg putByte: self class msgType.
        aMsg putByte: li.
        aMsg putByte: dat size.
        aMsg putByteArray: dat.
    ]

    data [
        ^ data
    ]
    data: aData [
        data := aData.
    ]

    sapi [
        ^ li bitAnd: 7
    ]

    linkIdentifier [
        ^ li
    ]
    linkIdentifier: aLi [
        li := aLi.
    ]
]
